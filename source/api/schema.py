from graphene import Schema, ObjectType

from api.graphql.queries import SongQuery, PlaylistQuery

from api.graphql.mutations import CreateSongMutation, CreatePlaylist, AppendSongMutation


class Query(SongQuery,
            ObjectType,
            PlaylistQuery):
    pass


class Mutation(CreateSongMutation,
               CreatePlaylist,
               AppendSongMutation,
               ObjectType):
    pass


schema = Schema(query=Query, mutation=Mutation)
