from django.db import models


class Song(models.Model):
    title = models.TextField(default="")
    author = models.TextField(default="")

    objects = models.Manager()

    def __str__(self):
        return f"<Song: [{self.id}] {self.title}>"
