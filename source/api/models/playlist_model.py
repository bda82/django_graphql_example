from django.db import models


class Playlist(models.Model):
    title = models.TextField(default="")
    songs = models.ManyToManyField("api.Song")

    objects = models.Manager()

    def __str__(self):
        return f"<Playlist: [{self.id}] {self.title}>"
