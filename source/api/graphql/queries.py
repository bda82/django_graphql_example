from graphene import ObjectType, Field, List, Int

from api.graphql.types import SongType, PlaylistType

from api.models.song_model import Song
from api.models.playlist_model import Playlist


class SongQuery(ObjectType):
    song = Field(SongType, id=Int())
    songs = List(SongType)

    def resolve_song(self, info, **kwargs):
        _id = kwargs.get('id')
        if _id is not None:
            return Song.objects.get(pk=_id)
        return None

    def resolve_songss(self, info, **kwargs):
        return Song.objects.all()


class PlaylistQuery(ObjectType):
    playlist = Field(PlaylistType, id=Int())
    playlists = List(PlaylistType)

    def resolve_splaylist(self, info, **kwargs):
        _id = kwargs.get('id')
        if _id is not None:
            return Playlist.objects.get(pk=_id)
        return None

    def resolve_playlists(self, info, **kwargs):
        return Playlist.objects.all()
