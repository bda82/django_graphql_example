from graphene import Mutation, Boolean, Field, ID

from api.models.song_model import Song
from api.models.playlist_model import Playlist

from api.graphql.inputs import SongInput
from api.graphql.inputs import PlaylistInput

from api.graphql.types import SongType
from api.graphql.types import PlaylistType


class CreateSongMutation(Mutation):
    class Arguments:
        input = SongInput(required=True)

    ok = Boolean()
    song = Field(SongType)

    @staticmethod
    def mutate(root, info, input):
        ok = True
        song_instance = Song(
            title=input.title,
            author=input.author
        )
        song_instance.save()
        return CreateSongMutation(ok=ok, song=song_instance)


class CreatePlaylist(Mutation):
    class Arguments:
        input = PlaylistInput(required=True)

    ok = Boolean()
    playlist = Field(PlaylistType)

    @staticmethod
    def mutate(root, info, input):
        ok = True
        playlist_instance = Playlist(
            title=input.title,
        )
        playlist_instance.save()
        return CreatePlaylist(ok=ok, playlist=playlist_instance)


class AppendSongMutation(Mutation):
    class Arguments:
        playlist_id = ID()
        song_id = ID()

    ok = Boolean()
    playlist = Field(PlaylistType)

    @staticmethod
    def mutate(root, info, playlist_id, song_id):
        ok = False
        playlist_instance = None
        song_instance = Song.objects.get(pk=song_id)
        if song_instance is not None:
            playlist_instance = Playlist.objects.get(pk=playlist_id)
            if playlist_instance is not None:
                if song_instance not in playlist_instance.songs.all():
                    playlist_instance.songs.add(song_instance)
                    playlist_instance.save()
                    ok = True
        return AppendSongMutation(ok=ok, playlist=playlist_instance)
