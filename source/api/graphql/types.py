from graphene_django.types import DjangoObjectType

from api.models.playlist_model import Playlist
from api.models.song_model import Song


class SongType(DjangoObjectType):
    class Meta:
        model = Song


class PlaylistType(DjangoObjectType):
    class Meta:
        model = Playlist
