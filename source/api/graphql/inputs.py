from graphene import ID, String, List
from graphene import InputObjectType


class SongInput(InputObjectType):
    id = ID()
    title = String()
    author = String()


class PlaylistInput(InputObjectType):
    id = ID()
    title = String()
    songs = List(SongInput)
